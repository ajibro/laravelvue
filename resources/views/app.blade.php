<!DOCTYPE html>
<html>
@include('sections.head')
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper" id="app">
    <app-component route="{{route('basepath')}}"></app-component>
  </div>
  @include('sections.script')
</body>
</html>
